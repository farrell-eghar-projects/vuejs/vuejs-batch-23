/* 
    soal 1

    buatlah variabel-variabel seperti di bawah ini

    var pertama = "saya sangat senang hari ini";
    var kedua = "belajar javascript itu keren";
    gabungkan variabel-variabel tersebut agar menghasilkan output

    saya senang belajar JAVASCRIPT
*/

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var bergabung = pertama.substr(0 , 5) + pertama.substring(12 , 19) + kedua.substring(0 , 8) + kedua.substr(8, 10).toUpperCase();

console.log(bergabung);

/*
    soal 2

    buatlah variabel-variabel seperti di bawah ini

    var kataPertama = "10";
    var kataKedua = "2";
    var kataKetiga = "4";
    var kataKeempat = "6";
    ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
    *catatan :
    1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
    2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)
*/

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var operasi = parseInt(kataKedua) * parseInt(kataKeempat) * parseInt(parseInt(kataPertama) / parseInt(kataKetiga));

console.log(operasi);

/*
    soal 3

    buatlah variabel-variabel seperti di bawah ini

    var kalimat = 'wah javascript itu keren sekali'; 

    var kataPertama = kalimat.substring(0, 3); 
    var kataKedua; // do your own! 
    var kataKetiga; // do your own! 
    var kataKeempat; // do your own! 
    var kataKelima; // do your own! 

    console.log('Kata Pertama: ' + kataPertama); 
    console.log('Kata Kedua: ' + kataKedua); 
    console.log('Kata Ketiga: ' + kataKetiga); 
    console.log('Kata Keempat: ' + kataKeempat); 
    console.log('Kata Kelima: ' + kataKelima);
    selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

    Kata Pertama: wah
    Kata Kedua: javascript
    Kata Ketiga: itu
    Kata Keempat: keren
    Kata Kelima: sekali
*/

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
