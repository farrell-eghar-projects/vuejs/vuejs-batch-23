/*
    Soal 1
    Judul : Function Penghasil Tanggal Hari Esok

    Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan 
    mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

    contoh 1

    var tanggal = 29
    var bulan = 2
    var tahun = 2020

    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

    contoh 2

    var tanggal = 28
    var bulan = 2
    var tahun = 2021

    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

    contoh 3

    var tanggal = 31
    var bulan = 12
    var tahun = 2020

    next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021
*/

function next_date(tgl, bln, thn) {
    var satuHari = 86400000;
    var date = new Date(thn + "-" + bln + "-" + tgl + " 11:11:11").getTime();
    var nextYear = new Date(date + satuHari).getFullYear();
    var nextMonth = new Date(date + satuHari).getMonth() + 1;
    var nextDate = new Date(date + satuHari).getDate();
    switch (nextMonth) {
        case 1 : { nextMonth = " Januari "; break; }
        case 2 : { nextMonth = " Februari "; break; }
        case 3 : { nextMonth = " Maret "; break; }
        case 4 : { nextMonth = " April "; break; }
        case 5 : { nextMonth = " Mei "; break; }
        case 6 : { nextMonth = " Juni "; break; }
        case 7 : { nextMonth = " Juli "; break; }
        case 8 : { nextMonth = " Agustus "; break; }
        case 9 : { nextMonth = " September "; break; }
        case 10 : { nextMonth = " Oktober " ; break; }
        case 11 : { nextMonth = " November "; break; }
        case 12 : { nextMonth = " Desember "; break; }
    }
    return console.log(nextDate + nextMonth + nextYear);
}

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun )

/*
    Soal 2
    Judul : Function Penghitung Jumlah Kata

    Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan
    nilai jumlah kata dalam kalimat tersebut.

    Contoh

    var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
    var kalimat_2 = "Saya Iqbal"


    jumlah_kata(kalimat_1) // 6
    jumlah_kata(kalimat_2) // 2
*/

function jumlah_kata(kata) {
    console.log(kata.trim().split(" ").length);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

jumlah_kata(kalimat_1)
jumlah_kata(kalimat_2)