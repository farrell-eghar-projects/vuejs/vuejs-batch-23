/*
    soal 1
    buatlah variabel seperti di bawah ini

    var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
    urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

    1. Tokek
    2. Komodo
    3. Cicak
    4. Ular
    5. Buaya
*/

var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var flag = 1;
while (flag <= 5) {
    for (var angka = 0; angka < 5; angka++) {
        if (parseInt(daftarBuah[angka].substr(0 , 1)) === flag) {
            console.log(daftarBuah[angka]);
            break;
        }
    }
    flag++;
}


/*
    soal 2
    Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat
    perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya
    hobby yaitu [hobby]!"


    Tulis kode function di sini

    
    var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
    
    var perkenalan = introduce(data)
    console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran,
    dan saya punya hobby yaitu Gaming" 
*/

function introduce (my) {
    return "Nama saya " + my.name + ", umur saya " + my.age + " tahun, alamat saya di " + my.address + ", dan saya punya hobby yaitu " + my.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
    
var perkenalan = introduce(data)
console.log(perkenalan)

/*
    soal 3
    Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian
    memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

    var hitung_1 = hitung_huruf_vokal("Muhammad")

    var hitung_2 = hitung_huruf_vokal("Iqbal")

    console.log(hitung_1 , hitung_2) // 3 2
*/

function hitung_huruf_vokal (string) {
    var angka = 0;
    var huruf;
    for (var i = 0; i < string.length; i++) {
        huruf = string.charAt(i).toUpperCase();
        if (huruf == "A" || huruf == "I" || huruf == "U" || huruf == "E" || huruf == "O") {
            angka++;
        }
    }
    return angka;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

/*
    soal 4

    Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai
    sebuah integer, dengan contoh input dan otput sebagai berikut.

    console.log( hitung(0) ) // -2
    console.log( hitung(1) ) // 0
    console.log( hitung(2) ) // 2
    console.log( hitung(3) ) // 4
    console.log( hitung(5) ) // 8
*/

function hitung (angka) {
    angka = angka * 2;
    return angka - 2;
}

console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )